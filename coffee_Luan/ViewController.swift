//
//  ViewController.swift
//  coffee_Luan
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit

import Alamofire

import Kingfisher

struct Cafe: Decodable {
    let file: String
}

class ViewController: UIViewController {
    
 
    @IBOutlet weak var ImagemCafe: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCafe()
    }
    
    var cafe: [Cafe]?
    
    
    @IBAction func MudarCafe(_ sender: Any) {
        getCafe()
    }
    
    func getCafe() {
        AF.request("https://coffee.alexflipnote.dev/random.json").responseDecodable(of: Cafe.self){
            response in
            if let cafe = response.value{
                self.ImagemCafe.kf.setImage(with: URL(string: cafe.file))
            }
        }
    }


}

